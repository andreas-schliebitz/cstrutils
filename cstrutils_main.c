#include <assert.h>
#include "cstrutils.h"

int main(void) {
    // TODO: Tests

    char *r = cstr_read("User input: ", 100, true);
    printf("%s\n", r);
    cstr_free(r);

    char s1[] = "Andreas";
    char s2[] = " Schliebitz";

    char *a[12] = {s1, s2};

    char *j = cstr_join(a, 2);
    printf("%s\n", j);
    cstr_free(j);

    char *f = cstr_format("%s is %d years old.", 35, "Andreas Schliebitz", 22);
    printf("%s\n", f);
    cstr_free(f);

    return EXIT_SUCCESS;
}
