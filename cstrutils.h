#ifndef CSTRUTILS_H
#define CSTRUTILS_H

#include <ctype.h>
#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CSTR_LOWERCASE "abcdefghijklmnopqrstuvwxyz"
#define CSTR_UPPERCASE "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define CSTR_LETTERS "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define CSTR_DIGITS "0123456789"
#define CSTR_HEXDIGITS "0123456789abcdefABCDEF"
#define CSTR_OCTDIGITS "01234567"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct cstr_parts {
    size_t count;
    char **parts;
} cstr_parts;

char *cstr_alloc(size_t length);
char *cstr_copy(const char *str);
void cstr_free(char *str);
bool cstr_valid_index(const char *str, size_t index);
size_t cstr_length(const char *str);
bool cstr_equal(const char *left, const char *right);
char cstr_at(const char *str, size_t index);
char *cstr_format(const char *format, size_t length, ...);
void cstr_lower(char *str);
void cstr_upper(char *str);
bool cstr_endswith(const char *str, char ch);
bool cstr_endswith_substr(const char *str, const char *substr);
bool cstr_startswith(const char *str, char ch);
bool cstr_startswith_substr(const char *str, const char *substr);
int64_t cstr_indexof(const char *str, char ch);
int64_t cstr_indexof_substr(const char *str, const char *substr);
bool cstr_contains(const char *str, char ch);
bool cstr_contains_substr(const char *str, const char *substr);
int64_t cstr_last_indexof(const char *str, char ch);
int64_t cstr_last_indexof_substr(const char *str, const char *substr);
void cstr_replace_all(char *str, char old, char new);
char *cstr_replace_all_substr(const char *str, const char *substr,
                              const char *new);
void cstr_replace_first(char *str, char old, char new);
void cstr_replace_last(char *str, char old, char new);
void cstr_trim(char *str);
cstr_parts *cstr_split(char *str, const char *delim);
void cstr_parts_free(cstr_parts *cstr_parts);
char *cstr_substr(const char *str, size_t start_index, size_t end_index);
void cstr_reverse(char *str);
char *cstr_concat(const char *left, const char *right);
size_t cstr_count_substr(const char *str, const char *substr);
size_t cstr_count(const char *str, char ch);
void cstr_capitalize(char *str);
bool cstr_isupper(const char *str);
bool cstr_islower(const char *str);
bool cstr_isalnum(const char *str);
bool cstr_isalpha(const char *str);
bool cstr_isdigit(const char *str);
void cstr_swapcase(char *str);
char *cstr_join(char **strs, size_t count);
char *cstr_join_parts(const cstr_parts *strp);
char *cstr_read(const char *prompt, size_t length, bool remove_newline);

#ifdef __cplusplus
}
#endif

#endif /* CSTRUTILS_H */
