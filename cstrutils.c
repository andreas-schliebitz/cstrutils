#include "cstrutils.h"

char *cstr_alloc(size_t length) { return calloc(length + 1, sizeof(char)); }

char *cstr_copy(const char *str) {
    if (!str) return NULL;
    char *cpy = cstr_alloc(cstr_length(str));
    if (!cpy) return NULL;
    return strcpy(cpy, str);
}

void cstr_free(char *str) {
    free(str);
    str = NULL;
}

bool cstr_valid_index(const char *str, size_t index) {
    return index < cstr_length(str);
}

size_t cstr_length(const char *str) {
    if (!str) return 0;
    return strlen(str);
}

bool cstr_equal(const char *left, const char *right) {
    if (!left || !right) return false;
    return !strcmp(left, right);
}

char cstr_at(const char *str, size_t index) {
    if (!str || !cstr_valid_index(str, index)) return '\0';
    return str[index];
}

char *cstr_format(const char *format, size_t length, ...) {
    char *str = cstr_alloc(length);
    if (!str) return NULL;
    va_list format_args;
    va_start(format_args, length);
    int n = vsprintf(str, format, format_args);
    va_end(format_args);
    if (n < 0) return NULL;
    return str;
}

void cstr_lower(char *str) {
    if (!str) return;
    for (size_t i = 0; str[i]; ++i) {
        str[i] = tolower(str[i]);
    }
}

void cstr_upper(char *str) {
    if (!str) return;
    for (size_t i = 0; str[i]; ++i) {
        str[i] = toupper(str[i]);
    }
}

bool cstr_endswith(const char *str, char ch) {
    const size_t len = cstr_length(str);
    if (!str || len == 0) return false;
    const char end = cstr_at(str, len - 1);
    return end == ch;
}

bool cstr_endswith_substr(const char *str, const char *substr) {
    if (!str || !substr) return false;
    size_t str_len = cstr_length(str);
    size_t substr_len = cstr_length(substr);
    if (substr_len > str_len || substr_len == 0) return false;
    return cstr_last_indexof_substr(str, substr) ==
           (int64_t)(str_len - substr_len);
}

bool cstr_startswith(const char *str, char ch) {
    if (!str || cstr_length(str) == 0) return false;
    const char start = cstr_at(str, 0);
    return start == ch;
}

bool cstr_startswith_substr(const char *str, const char *substr) {
    if (!str || !substr) return false;
    return cstr_indexof_substr(str, substr) == 0;
}

int64_t cstr_indexof(const char *str, char ch) {
    if (!str) return -1;
    for (size_t i = 0; str[i]; ++i) {
        if (str[i] == ch) return i;
    }
    return -1;
}

int64_t cstr_indexof_substr(const char *str, const char *substr) {
    if (!str || !substr) return -1;
    const size_t substr_len = cstr_length(substr);
    if (substr_len == 0) return -1;
    const size_t str_len = cstr_length(str);
    if (substr_len > str_len) return -1;
    for (size_t i = 0; i + substr_len - 1 < str_len; ++i) {
        char *s = cstr_substr(str, i, i + substr_len - 1);
        if (cstr_equal(s, substr)) {
            cstr_free(s);
            return i;
        }
        cstr_free(s);
    }
    return -1;
}

bool cstr_contains(const char *str, char ch) {
    if (!str) return false;
    return cstr_indexof(str, ch) != -1;
}

bool cstr_contains_substr(const char *str, const char *substr) {
    if (!str || !substr) return false;
    return cstr_indexof_substr(str, substr) != -1;
}

int64_t cstr_last_indexof(const char *str, char ch) {
    if (!str) return -1;
    const size_t len = cstr_length(str);
    for (int64_t i = len - 1; i >= 0; --i) {
        if (str[i] == ch) return i;
    }
    return -1;
}

int64_t cstr_last_indexof_substr(const char *str, const char *substr) {
    if (!str || !substr) return -1;
    const size_t substr_len = cstr_length(substr);
    if (substr_len == 0) return -1;
    const size_t str_len = cstr_length(str);
    if (substr_len > str_len) return -1;
    for (int64_t i = str_len - substr_len; i >= 0; --i) {
        char *s = cstr_substr(str, i, i + substr_len - 1);
        if (cstr_equal(s, substr)) {
            cstr_free(s);
            return i;
        }
        cstr_free(s);
    }
    return -1;
}

void cstr_replace_all(char *str, char old, char new) {
    if (!str) return;
    for (size_t i = 0; str[i]; ++i) {
        if (str[i] == old) str[i] = new;
    }
}

char *cstr_replace_all_substr(const char *str, const char *substr,
                              const char *new) {
    if (!str || !substr || !new) return NULL;
    const size_t substr_len = cstr_length(substr);
    const int64_t len_diff =
        (cstr_length(new) - substr_len) * cstr_count_substr(str, substr);
    const size_t new_str_len = cstr_length(str) + len_diff;
    char *new_str = cstr_alloc(new_str_len);
    if (!new_str) return NULL;
    size_t s = 0;
    for (size_t i = 0; str[i]; ++i) {
        char *cur_str = cstr_substr(str, i, i + substr_len - 1);
        if (cstr_equal(cur_str, substr)) {
            for (size_t j = 0; new[j]; ++j) {
                new_str[s++] = new[j];
            }
            i += substr_len - 1;
        } else {
            new_str[s++] = str[i];
        }
        cstr_free(cur_str);
    }
    new_str[s] = '\0';
    return new_str;
}

void cstr_replace_first(char *str, char old, char new) {
    if (!str) return;
    for (size_t i = 0; str[i]; ++i) {
        if (str[i] == old) {
            str[i] = new;
            break;
        }
    }
}

void cstr_replace_last(char *str, char old, char new) {
    if (!str) return;
    const size_t len = cstr_length(str);
    for (int64_t i = len - 1; i >= 0; --i) {
        if (str[i] == old) {
            str[i] = new;
            break;
        }
    }
}

void cstr_trim(char *str) {
    if (!str) return;
    while (cstr_startswith(str, ' ')) {
        for (size_t i = 0; str[i]; ++i) {
            str[i] = str[i + 1];
        }
    }
    while (cstr_endswith(str, ' ')) {
        size_t len = cstr_length(str);
        str[len - 1] = '\0';
    }
}

cstr_parts *cstr_split(char *str, const char *delim) {
    if (!str || !delim) return NULL;
    cstr_parts *cstr_p = calloc(1, sizeof(cstr_parts));
    if (!cstr_p) return NULL;
    char *pch = strtok(str, delim);
    while (pch) {
        char *part = cstr_copy(pch);
        if (!part) return NULL;
        cstr_p->parts =
            realloc(cstr_p->parts, (cstr_p->count + 1) * sizeof(char *));
        if (!cstr_p->parts) return NULL;
        cstr_p->parts[cstr_p->count] = part;
        cstr_p->count++;
        pch = strtok(NULL, delim);
    }
    return cstr_p;
}

void cstr_parts_free(cstr_parts *cstr_parts) {
    if (!cstr_parts || !cstr_parts->parts) return;
    for (size_t i = 0; i < cstr_parts->count; ++i) {
        cstr_free(cstr_parts->parts[i]);
    }
    free(cstr_parts->parts);
    cstr_parts->parts = NULL;
    free(cstr_parts);
    cstr_parts = NULL;
}

char *cstr_substr(const char *str, size_t start_index, size_t end_index) {
    if (!str || start_index > end_index || !cstr_valid_index(str, end_index))
        return NULL;
    char *substr = cstr_alloc(end_index - start_index + 1);
    if (!substr) return NULL;
    size_t s = 0;
    for (size_t i = start_index; i <= end_index; ++i) {
        substr[s++] = str[i];
    }
    substr[s] = '\0';
    return substr;
}

void cstr_reverse(char *str) {
    if (!str) return;
    const size_t len = cstr_length(str);
    if (len == 0) return;
    size_t end = len - 1;
    for (size_t i = 0; i < len / 2; ++i) {
        char tmp = str[i];
        str[i] = str[end];
        str[end--] = tmp;
    }
}

char *cstr_concat(const char *left, const char *right) {
    if (!left || !right) return NULL;
    const size_t len = cstr_length(left) + cstr_length(right);
    char *conc = cstr_alloc(len);
    if (!conc) return NULL;
    strcat(strcpy(conc, left), right);
    return conc;
}

size_t cstr_count(const char *str, char ch) {
    if (!str) return 0;
    size_t ch_count = 0;
    for (size_t i = 0; str[i]; ++i) {
        if (str[i] == ch) ch_count++;
    }
    return ch_count;
}

size_t cstr_count_substr(const char *str, const char *substr) {
    if (!str || !substr) return 0;
    const size_t substr_len = cstr_length(substr);
    if (substr_len == 0) return 0;
    const size_t str_len = cstr_length(str);
    if (substr_len > str_len) return 0;
    size_t substr_count = 0;
    for (size_t i = 0; i + substr_len - 1 < str_len; ++i) {
        char *s = cstr_substr(str, i, i + substr_len - 1);
        if (cstr_equal(s, substr)) {
            substr_count++;
        }
        i += substr_len - 1;
        cstr_free(s);
    }
    return substr_count;
}

void cstr_capitalize(char *str) {
    if (!str || cstr_length(str) == 0) return;
    cstr_lower(str);
    str[0] = toupper(str[0]);
}

bool cstr_isupper(const char *str) {
    if (!str || cstr_length(str) == 0) return false;
    for (size_t i = 0; str[i]; ++i) {
        if (str[i] != toupper(str[i])) return false;
    }
    return true;
}

bool cstr_islower(const char *str) {
    if (!str || cstr_length(str) == 0) return false;
    for (size_t i = 0; str[i]; ++i) {
        if (str[i] != tolower(str[i])) return false;
    }
    return true;
}

bool cstr_isalnum(const char *str) {
    if (!str || cstr_length(str) == 0) return false;
    for (size_t i = 0; str[i]; ++i) {
        if (!cstr_contains(CSTR_LETTERS, str[i]) &&
            !cstr_contains(CSTR_DIGITS, str[i]))
            return false;
    }
    return true;
}

bool cstr_isalpha(const char *str) {
    if (!str || cstr_length(str) == 0) return false;
    for (size_t i = 0; str[i]; ++i) {
        if (!cstr_contains(CSTR_LETTERS, str[i])) return false;
    }
    return true;
}

bool cstr_isdigit(const char *str) {
    if (!str || cstr_length(str) == 0) return false;
    for (size_t i = 0; str[i]; ++i) {
        if (!cstr_contains(CSTR_DIGITS, str[i])) return false;
    }
    return true;
}

void cstr_swapcase(char *str) {
    if (!str) return;
    for (size_t i = 0; str[i]; ++i) {
        if (str[i] == tolower(str[i])) {
            str[i] = toupper(str[i]);
        } else {
            str[i] = tolower(str[i]);
        }
    }
}

char *cstr_join(char **strs, size_t count) {
    if (!strs) return NULL;
    size_t new_str_len = 0;
    for (size_t i = 0; i < count; ++i) {
        if (strs + i && strs[i]) {
            new_str_len += cstr_length(strs[i]);
        }
    }
    char *new_str = cstr_alloc(new_str_len);
    size_t s = 0;
    for (size_t i = 0; i < count; ++i) {
        if (strs + i && strs[i]) {
            for (size_t j = 0; strs[i][j]; ++j) {
                new_str[s++] = strs[i][j];
            }
        }
    }
    new_str[s] = '\0';
    return new_str;
}

char *cstr_join_parts(const cstr_parts *strp) {
    if (!strp) return NULL;
    return cstr_join(strp->parts, strp->count);
}

char *cstr_read(const char *prompt, size_t length, bool remove_newline) {
    char *str = cstr_alloc(length);
    if (prompt) printf("%s", prompt);
    fgets(str, length + 1, stdin);
    if (remove_newline) cstr_replace_last(str, '\n', '\0');
    return str;
}
